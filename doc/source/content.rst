********
ZoomView
********

**ZoomView is a QGIS plugin adding a Zoom Panel showing a magnification of the map canvas around a selected point.**
**Map tools are still usable in the map canvas, and in the ZoomView as well.**

.. contents:: Table of Content
    :local:

Getting started
===============

Installation
    In QGIS select :menuselection:`QGIS --> Plugins --> Manage and Install Plugins...`,
    search for ``ZoomView`` and install the plugin.

Start
    In the toolbar click |icon| to show the :guilabel:`ZoomView` panel.

Setup an Image-Overview-Zoom map view arrangement
    - Open a raster layer.
    - Right click on it and |mActionInOverview| :sup:`Show in Overview`.
    - |mActionZoomActual| :sup:`Zoom to Native Resolution`.
    - Activate |mActionIdentify| :sup:`Identify Features` map tool.
    - Select an interesting area inside the map canvas.

    The magnified extent (red rectangle) is centered around the selected pixel (red dot/crosshair).

    .. figure:: screenshot.png

         Image-Overview-Zoom map view arrangement with Landsat 8 image in true-color composition at native resolution zoom (left),
         overview zoom (upper right) and zoom view with x5 magnification (lower right).

    .. figure:: screenshot2.png

        Spectral profile plot for the selected pixel.

.. |icon| image:: icon.svg
   :height: 24
   :width: 24

.. |mActionZoomActual| image:: mActionZoomActual.svg
   :height: 24
   :width: 24

.. |mActionInOverview| image:: mActionInOverview.svg
   :height: 24
   :width: 24

.. |mActionIdentify| image:: mActionIdentify.svg
   :height: 24
   :width: 24

.. admonition:: Info

    For comparison: in the **ENVI Classic** raster processing software, an Image-Overview-Zoom map view arrangement looks like this.

    .. figure:: screenshot3.png

Main Features
=============

The main features of **ZoomView** are:

Image-Overview-Zoom map view arrangement
    The :guilabel:`ZoomView` panel complements the QGIS :guilabel:`Overview` panel with functionality for magnifying specific map regions.
    This allows a Image-Overview-Zoom map view arrangement similar to traditionally raster processing software like **ENVI Classic**.

Map tool agnostic
    **ZoomView** is designed to not interfear with the usage of other map tools.
    Selecting the zoom view center point does not require a specific map tool,
    but works with any available map tool.
    Otherwise would the plugin not be very useful at all.

    .. important::

        Note that technically **ZoomView** comes with it's own map tool, but the user never selects it directly.
        When the user selects a new map tool, it is replaced by the **ZoomView** map tool,
        which is wrapping the user-selected map tool.
        All canvas events will be redirected to the user-selected map tool to perform the proper actions.
        The only purpose of the **ZoomView** map tool is to update the zoom view center point whenever the user performs a mouse button click.

        This is kind of a hacky way to enable any map tool inside the map canvas, while at the same time be able to update the zoom view.

        If anybody has a better solution for that, please get in contact.

Map tool functionality inside zoom view
    A currently selected map canvas map tool will also work inside the zoom view.

    .. important::

        Technically, the map tool used in the map canvas is not really set to the zoom view,
        but all zoom view canvas events (i.e. :class:`QgsMapMouseEvent`) are catched,
        map coordinates are converted into proper map canvas device coordinates,
        and passed to the corresponding map canvas map tool.
        This way, interacting with the zoom view, is like indirectly interacting with the map canvas.

        This solution is also kind of hacky, but it works just fine.

        A better solution would be to clone the existing map tool instance and change the assigned map canvas to the zoom view.
        If anybody knows how to properly do this for any given map tool instance, please get in contact.

Precise center point selection and preservation
    While using any map tool, a left or right mouse click will trigger the map tool functionality,
    but also select a new zoom view center point.
    Exceptions to this rule are the
    |mActionPan| :sup:`Pan Map`, |mActionZoomIn| :sup:`Zoom In` and |mActionZoomOut| :sup:`Zoom Out` map tools,
    where only the right mouse is functional.
    This way, panning and zooming inside the map canvas will not change the selected center point.

.. |mActionPan| image:: mActionPan.svg
   :height: 24
   :width: 24
.. |mActionZoomIn| image:: mActionZoomIn.svg
   :height: 24
   :width: 24
.. |mActionZoomOut| image:: mActionZoomOut.svg
   :height: 24
   :width: 24

Pixel-wise navigation
    The :kbd:`A`, :kbd:`W`, :kbd:`S`, :kbd:`D` keys can be used to move the zoom view center to one of the neighbouring pixels.
    and trigger a left mouse button click with the map tool.
    The underlaying pixel grid is defined by the currently selected raster layer in the :guilabel:`Layers` panel.

Center point focussed on zooming
    The zoom view center point stays centered while the user zooms in or out.
    This helps to keep the focus on the selected center point, while adjusting the zoom level.

Connect to point-changed-signal
===============================

Whenever a new zoom view center point is selected, a signal is emitted.

You may for example connect to that signal and print every selected point.

In the :guilabel:`QGIS Python Console execute`::

    >>> import zoomview
    >>> zoomview.zvInterface().sigPointChanged.connect(lambda p: print(p))

From now on, each selected center point is printed, e.g.::

    <QgsPointXY: POINT(399741.25697211141232401 5831045.63844621554017067)>

Contact
=======

Please provide feedback to `Andreas Rabe`_ (andreas.rabe\@geo.hu-berlin.de)
or `create an issue`_.

.. _Andreas Rabe: https://www.geographie.hu-berlin.de/de/Members/rabe_andreas
.. _create an issue: https://bitbucket.org/janzandr/zoomview/issues/new

.. codeauthor:: Andreas Rabe <andreas.rabe@geo.hu-berlin.de>
.. sectionauthor:: Andreas Rabe <andreas.rabe@geo.hu-berlin.de>
